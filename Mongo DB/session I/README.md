```sh
# use it in the command line to start the main Mongo DB, running on the localhost under default port number
mongo

# connect to Mongo DB server with a specific port number
mongo --port [port number]
```

```js
// list of all database
show dbs

// select and start using a database
use ( name_of_the_database )

// you can get the name of selected database
db

// drop the already selected database
db.dropDatabase()

// list all collection of the already selected database
show collections

// create a new collection under the selected database
db.createCollection( 'name of the collection' )
```




**INSERT**  
```JS
db.posts.insert({
  title: 'First title',
  author: 'First author',
  conten: 'Lorem Ipsum'
})
```




**INSERT** - multi  
```JS
db.posts.insertMany([
  {
    title: 'First title',
    author: 'First author',
    conten: 'Lorem Ipsum'
  },
  {
    title: 'Second title',
    author: 'Second author',
    content: 'Lorem Ipsum'
  }
])
```




**FIND**  
```js
// select all the elements
db.posts.find()

// select all the elements and format them
db.posts.find().pretty()

// select all the elements by the conditions
db.posts.find({ title: 'First title' })

// select all the elements by the conditions and sort them in alphabetical order
// ASC = 1
// DESC = -1
db.posts.find({ title: 'First title' }).sort({ title: 1 })

// select all but show only 2 elements
db.posts.find().limit( 2 )

// select all elements by the conditions but show only the first one
db.posts.findOne({ title: 'First title' })

// select all but show only the count value
db.posts.find().count()

// select all and make preparation
db.posts.find().forEach(function(doc) { 
  print('Blog post: ' + doc.title) 
})

// output:
// Blog post: First title
// Blog post: Second title
```

**FIND** - elemMatch operator  
*(make selection by element of an array)*
```json
[{
  "title": "Foo Bar Post",
  "comments": [
    {
      "name": "Person First",
      "content": "Lorem Ipsum"
    },
    {
      "name": "Person Second",
      "content": "Lorem Ipsum"
    }
  ]
},{
  "title": "Another Post",
  "comments": [
    {
      "name": "Anonym First",
      "content": "Lorem Ipsum"
    },
    {
      "name": "Anonym Second",
      "content": "Lorem Ipsum"
    }
  ]
}]
```

```js
// Let's say we have docs like above in our collection and we would like to find all the posts where 'Person First' commented something.
const condition = { 
  comments: {
    $elemMatch: {
      name: "Person First"
    }
  }
}

db.posts.find( condition )
```




**FIND** - search operator  
*(make selection by text search)*
```json
[
  { "title": "Post First" }
  { "title": "Post Second" }
  { "title": "Post Third" }
]
```

```js
// Let's say we have docs like above in our collection and we would like to find all the posts where title prop have this text piece: 'Post F'

// fist we need to make an index on title props
db.posts.createIndex({ title: 'text' })

// then we can make search with find
db.posts.find({ 
  $text: {
    $search: "\"Post F\""
  }
})
```




**FIND** - search operator  
*(make selection by text search)*
```json
[
  { "title": "Post First" }
  { "title": "Post Second" }
  { "title": "Post Third" }
]
```

```js
// Let's say we have docs like above in our collection and we would like to find all the posts where title prop have this text piece: 'Post F'

// fist we need to make an index on title props
db.posts.createIndex({ title: 'text' })

// then we can make search with find
db.posts.find({ 
  views: {
    $gt: 3
  }
})
```




**UPDATE**
```js
// every element going to be replaced which has 'Foo' as title value...
const condition = { title: 'Foo' }
// ... with this new value...
const newValue = { title: 'Bar', category: 'none' }
// ... if there is no element with condition then we will create it
const options = { upsert: true }

// UPDATE command
// (the properties what are not appear in the newValue variable will be lost)
db.posts.update( condition, newValue, options )
```




**UPDATE** - set operator  
*(change only selected props)*
```js
// every element going to be replaced which has 'Foo' as title value...
const condition = { title: 'Foo' }
// ... with this new value...
const newValue = { 
  $set: {
    title: 'Bar' 
  }
}
// ... if there is no element with condition then we will create it
const options = { upsert: true }

// UPDATE command
db.posts.update( condition, newValue, options )
```




**UPDATE** - inc operator  
*(increment number type of value)*
```js
// every element going to be replaced which has 'Foo' as title value...
const condition = { title: 'Foo' }
// ... with this new value...
const newValue = { 
  $inc: {
    // lets say we have a like prop already with value of 1
    // ... and we would like to increase its value with 2
    likes: 2
  }
}
// ... if there is no element with condition then we will create it

// UPDATE command
db.posts.update( condition, newValue )
```




**UPDATE** - rename operator  
*(change the name of the prop)*
```js
// every element going to be replaced which has 'Foo' as title value...
const condition = { title: 'Foo' }
// ... with this new value...
const newValue = { 
  $inc: {
    // [old prop name]: [new prop name]
    likes: views
  }
}
// ... if there is no element with condition then we will create it

// UPDATE command
db.posts.update( condition, newValue )
```




**DELETE**  
```js
// we just need to set the condition
db.posts.remove({
  title: 'POST Four'
})
```