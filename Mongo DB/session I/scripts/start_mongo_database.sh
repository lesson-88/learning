docker run \
  --rm \
  -d \
  -p 27017:27017 \
  --name mongodb \
  -v "$(pwd)/db:/data/db" \
  mongo:latest